<!-- Template for requests for help from the Support team for the Reference Architecture group -->

<!---
Before raising please note the following:
- Requests should strictly only be raised by the Support Team for assistance from the Reference Architecture group when an issue is suspected to be due to environmental design - https://handbook.gitlab.com/handbook/support/workflows/how-to-get-help/#how-to-use-gitlabcom-to-formally-request-help-from-the-gitlab-development-team
- All sections marked Required must be filled out in full. Reviews will not proceed until provided.
--->

### Customer Details

* Salesforce Link:
* Account Management Project link:
* SA Owner:
* TAM Owner:
* Support Ticket (if applicable):

### Environment Details (Required)

<!---
This section is required. Reviews will *not* proceed until information is given.
--->

* **Target hardware provider (GCP, AWS, other)**:
* **Number of users (current / target)**:
* **Number of CI runners or agents (current / target)**:
* **GitLab version (current / target)**:
* **Environment Diagram with Nodes and Specs**:

### Request Details (Required)

<!---
Please provide details for the specific request for help that requires dedicated time from a Reference Architecture group member.
A summary of what investigations has occurred and what they have found is required to get us up to speed.
We aim to respond to requests within 2 weeks.
--->

### GitLab Performance Tool (GPT) results (Optional)

<!---
Please provide GPT results if the customer has them to evaluate performance.
--->

### Relevant environment metrics if existing (CPU / Memory)

<!---
Please provide any relevant environment metrics if this is an existing environment to help with performance analysis, such as from a GPT run. Primarily this would be CPU, Memory or IO but can include any metrics that potentially show an issue.
--->

### Author's checklist

* [ ] Request is from Support to review an existing environment that's design is suspected to be a factor in an issue.
* [ ] [Customer Details](#customer-details) section filled out.
* [ ] [Environment Details](#environment-details-required) section filled out.
* [ ] [Request Details](#request-details-required) section filled out with summary information given from Support on investigations done and findings found that suggested an environmental issue.
* [ ] [GPT results](#gitlab-performance-tool-gpt-results-optional) added (Optional).
* [ ] [Environment metrics](#relevant-environment-metrics-if-existing-cpu--memory) added (Optional).
* [ ] Set the ~"workflow::ready for review" label when all required details are given.

/label ~"Reference Architecture" ~"Self-Managed Platform::request for help"  ~"Self-Managed Excellence" ~"workflow::refinement"
/confidential

<!---
Before raising please note the following:
- Requests should strictly only be made for help with issues suspected to be due to existing environment design, not already covered in the [Reference Architecture docs](https://docs.gitlab.com/ee/administration/reference_architectures/).
  - Requests should contain a full summary from Support of all investigations done and findings found.
- **This is a cross department group and provides reviews for environment designs on a best effort basis. The group is not a dedicated or an escalation resource and is unable to fulfil any urgent requests or attend customer calls. The group is expressly not a replacement for the Support or Professional Services teams**
- This group cannot offer any specific security recommendations for customers due to legal considerations.

The Reference Architecture group will seek to review within 2 weeks but please note this is not guaranteed.
--->
