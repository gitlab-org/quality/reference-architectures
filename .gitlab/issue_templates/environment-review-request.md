<!---
Before raising please note the following:
- Reviews should strictly only be raised for sanity checks of new environment designs, not already covered in the [Reference Architecture docs](https://docs.gitlab.com/ee/administration/reference_architectures/).
- For support requests pertaining to existing environments, where issues are suspected to be due to environmental design, please use the Support Request template.
- For security reviews this should be raised with the Security Architecture team - https://about.gitlab.com/handbook/security/architecture/review.html
- For GitLab Runner architecture reviews this should be raised with the Runner team - https://about.gitlab.com/handbook/engineering/development/ops/verify/runner/#how-to-work-with-us
- All sections marked Required must be filled out in full. Reviews will not proceed until provided.
--->

### Customer Details

* Salesforce Link:
* Account Management Project link:
* SA Owner:
* TAM Owner:
* Support Ticket (if applicable):

### Environment Details (Required)

<!---
This section is required. Reviews will *not* proceed until information is given.
--->

* **Target hardware provider (GCP, AWS, other)**:
* **Number of users (current / target)**:
* **Number of CI runners or agents (current / target)**:
* **Approx expected peak throughput (Requests Per Second - RPS)**:
* **Approx expected types of usage (API, Web, Git, Git LFS)**:
* **GitLab version (current / target)**:
* **Approximate number of repositories**:
* **Number of [large repositories](https://docs.gitlab.com/ee/user/project/repository/managing_large_repositories.html#managing-large-repositories) 5 GB or more**:
* **Environment Diagram with Nodes and Specs**:

### Git Sizer outputs for all large repositories 5 GB or more (Required)

<!---
Please provide git-sizer outputs of any large repositories as these can dramatically effect the environment:

GitLab Documentation - https://docs.gitlab.com/ee/user/project/repository/monorepos/#profiling-repositories
git-sizer project - https://github.com/github/git-sizer

Sizer outputs should be given in collapsible sections for formatting.

This section is required. Reviews will *not* proceed until information is given.
--->

### Reason for Review (Required)

<!---
Please give the reason for the environment review

This section is required. Reviews will *not* proceed until information is given.
--->

### Author's checklist

* [ ] Request is for a sanity check of an environment that is not already covered by the [Reference Architecture docs](https://docs.gitlab.com/ee/administration/reference_architectures/) including docs on [supported modifications](https://docs.gitlab.com/ee/administration/reference_architectures/3k_users.html#supported-modifications-for-lower-user-counts-ha).
* [ ] [Customer Details](#customer-details) section filled out.
* [ ] [Environment Details](#environment-details-required) section filled out.
* [ ] [Git Sizer outputs for all large repositories 5 GB or more](#git-sizer-outputs-for-all-large-repositories-5-gb-or-more-required) added.
* [ ] [Reason for Review](#reason-for-review-required) section filled out.
* [ ] Set the ~"workflow::ready for review" label when all required details are given.

<!---
Thank you! The Reference Architecture group will seek to review within 2 weeks but please note this is not guaranteed.
- **This is a cross department group and provides reviews for environment designs on a best effort basis. The group is not a dedicated or an escalation resource and is unable to fulfill any urgent requests or attend customer calls. The group is expressly not a replacement for the Support or Professional Services teams**
- This group cannot offer any specific security recommendations for customers due to legal considerations.
--->
/confidential
/label ~"ReferenceArchitecture::to review" ~"Self-Managed Excellence"
/label ~"workflow::refinement" 