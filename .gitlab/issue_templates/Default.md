**Please read before proceeding**

Requests to the Reference Architecture group can be for the following reasons:

- Environment Review - Requests to review new environment designs that aren't already covered in the [Reference Architecture docs](https://docs.gitlab.com/ee/administration/reference_architectures/)
- Request for Help - Requests raised by the Support Team for assistance with issues when environmental design is suspected to be a factor.

**For both types of request the appropriate template must be selected and filled in as directed**

Finally, please note this is a cross department group and provides reviews for environment designs on a best effort basis. The group is not a dedicated or an escalation resource and is unable to fulfil any urgent requests, attend customer calls or join customer projects.

The group is expressly not a replacement for the Support or Professional Services teams.
