# Triage process

To track issues in the Reference Architectures backlog we use the following label types:

- ~"ReferenceArchitecture::to review" - request to review customers' environment
- ~"ReferenceArchitecture::gpt results review" - request to review GPT results
- ~"ReferenceArchitecture::maintenance" - maintenance work for keeping Reference Architectures up-to-date i.e. documentation updates, validation of components or services
- ~"ReferenceArchitecture::question" - general questions about Reference Architectures
- ~"Self-Managed Platform::request for help" - Template for requests for help from the Support team for the Self Managed Platform team

To track the status of issues we follow the general workflow labels:

- ~"workflow::refinement" - Review request is awaiting required details to be given
- ~"workflow::ready for review" - waits for review from SME team side
- ~"workflow::in review" - issue is in review
- ~"workflow::in review" + ~"awaiting feedback" - awaiting feedback from customer, no action items from SME team
- ~"workflow::blocked" + ~"awaiting feedback" - awaiting feedback from customer, can't proceed review (for example, missing customer environment information)
- ~"workflow::blocked" - blocked by issue outside of RA
- ~"workflow::ready for development" - ready for development for testing/validation issues

Upon closing of Reference Architecture issue, the following label types are used to track conclusion:
- ~"RefArchReview::documentation update" - Issue closed after a documentation update
- ~"RefArchReview::redirected" - Issue closed because discussion has been moved to another team or request was not applicable (ie support request)
- ~"RefArchReview::refer to documentation" - Issue closed due to question being answered and/or referencing back to our existing documentation

By default, Reference Architecture review templates have ~"ReferenceArchitecture::to review" and ~"workflow::ready for review" labels. When an issue is in review - ~"workflow::in review" should be applied. Once a review is done, ~"workflow::in review" + ~"awaiting feedback" can be set. 
